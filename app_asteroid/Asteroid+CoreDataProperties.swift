//
//  Asteroid+CoreDataProperties.swift
//  app_asteroid
//
//  Created by developer on 07.02.17.
//  Copyright © 2017 Mikolas. All rights reserved.
//

import Foundation
import CoreData


extension Asteroid {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Asteroid> {
        return NSFetchRequest<Asteroid>(entityName: "Asteroid");
    }

    @NSManaged public var name: String?
    @NSManaged public var date: NSDate?
    @NSManaged public var atpos: Double
    @NSManaged public var lonpos: Double
    @NSManaged public var weigth: Double
    @NSManaged public var rclass: String?
    @NSManaged public var id: Int32

}
