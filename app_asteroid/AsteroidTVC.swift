//
//  AsteroidTVC.swift
//  app_asteroid
//
//  Created by developer on 07.02.17.
//  Copyright © 2017 Mikolas. All rights reserved.
//

import UIKit

class AsteroidTVC: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
