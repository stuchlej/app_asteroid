//
//  DetailTVCon.swift
//  app_asteroid
//
//  Created by developer on 07.02.17.
//  Copyright © 2017 Mikolas. All rights reserved.
//

import UIKit
import MapKit

/*
 Asteroid detail view controller
 */

class DetailTVCon: UITableViewController, MKMapViewDelegate {

    var asteroid:Asteroid!
    
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var classLabel: UILabel!
    @IBOutlet weak var weigthLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var siteLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.mapView.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        let asteroidLocation = CLLocationCoordinate2DMake(self.asteroid.atpos, self.asteroid.lonpos)
        let dropPin = MKPointAnnotation()
        dropPin.coordinate = asteroidLocation
        mapView.addAnnotation(dropPin)
        mapView.setCenter(asteroidLocation, animated: false)
        
        self.nameLabel.text = self.asteroid.name
        self.idLabel.text = String(self.asteroid.id)
        self.classLabel.text = self.asteroid.rclass
        self.weigthLabel.text = String(self.asteroid.weigth) + " g"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy"
        self.yearLabel.text = dateFormatter.string(from: self.asteroid.date as! Date)
        self.siteLabel.text = self.coordinateString(latitude: self.asteroid.atpos, longitude: self.asteroid.lonpos)
    }

    //převzatý kód
    func coordinateString(latitude:Double, longitude:Double) -> String {
        var latSeconds = Int(latitude * 3600)
        let latDegrees = latSeconds / 3600
        latSeconds = abs(latSeconds % 3600)
        let latMinutes = latSeconds / 60
        latSeconds %= 60
        var longSeconds = Int(longitude * 3600)
        let longDegrees = longSeconds / 3600
        longSeconds = abs(longSeconds % 3600)
        let longMinutes = longSeconds / 60
        longSeconds %= 60
        return String(format:"%d°%d'%d\"%@ %d°%d'%d\"%@",
                      abs(latDegrees),
                      latMinutes,
                      latSeconds,
                      {return latDegrees >= 0 ? "N" : "S"}(),
                      abs(longDegrees),
                      longMinutes,
                      longSeconds,
                      {return longDegrees >= 0 ? "E" : "W"}() )
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
