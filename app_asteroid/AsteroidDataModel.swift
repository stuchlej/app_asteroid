//
//  AsteroidDataModel.swift
//  app_asteroid
//
//  Created by developer on 06.02.17.
//  Copyright © 2017 Mikolas. All rights reserved.
//

import UIKit
import CoreData

protocol AsteroidDataModelDelegate {

    func asteroidModel(_ model:AsteroidDataModel, found items:Array<Asteroid>)
    func asterodiModel(_ model:AsteroidDataModel, failedWith error:(code:Int, desc:String))
    
}

class AsteroidDataModel: NSObject {

    //remote server constants
    let nasaURL = "https://data.nasa.gov/resource/y77d-th95.json"
    let token = "rAYraRXDf4t9LhzCYfS9KUaq4"
    let startYear = "2001"
    
    //class variables
    var managedObjectContext: NSManagedObjectContext!
    var delegate: AsteroidDataModelDelegate?
    var autoupdateTimer : Timer?
    
    init(withCoreDataModel: NSManagedObjectContext!){
        super.init()
        managedObjectContext = withCoreDataModel
    }
    
    //update database with high priority
    func updateDatabase(){
        self.downloadFromRemote(userInitiadet: true)
    }
    
    //autoupdating support
    func scheduleAutoupdating(){
        if(self.autoupdateTimer != nil){
            return
        }
        
        guard let lastUpdateDate = UserDefaults.standard.object(forKey: "kLastUpdate") as! Date! else {
            return
        }
        
        let futureUpdateDate = lastUpdateDate.addingTimeInterval(60 * 60 * 24)
        
        if (futureUpdateDate < Date()) {
            self.autoupdateTimer = Timer.scheduledTimer(timeInterval: futureUpdateDate.timeIntervalSinceNow, target: self, selector: #selector(AsteroidDataModel.fireAutoupdate), userInfo: nil, repeats: false)
        }
    }
    
    func invalidateAutoupdating(){
        if (self.autoupdateTimer != nil) {
            self.autoupdateTimer?.invalidate()
            self.autoupdateTimer = nil
        }
    }
    
    func fireAutoupdate(){
        self.downloadFromRemote(userInitiadet: false)
        
        let futureUpdateDate = Date().addingTimeInterval(60 * 60 * 24)
        
        self.autoupdateTimer = Timer.scheduledTimer(timeInterval: futureUpdateDate.timeIntervalSinceNow, target: self, selector: #selector(AsteroidDataModel.fireAutoupdate), userInfo: nil, repeats: false)
    }
    
    //get data from database, ordered
    func getData() -> Array<Asteroid>{
        let fetchRequest = NSFetchRequest<Asteroid>(entityName: "Asteroid")
        
        let sortDescriptor = NSSortDescriptor(key: "weigth", ascending: false)
        let sortDescriptors = [sortDescriptor]
        fetchRequest.sortDescriptors = sortDescriptors
        
        var asteroidsInDb: Array<Asteroid>
        do {
            asteroidsInDb = try managedObjectContext.fetch(fetchRequest)
        } catch {
            return Array()
        }
        
        return asteroidsInDb
    }
    
    //downloading and parsing
    func downloadFromRemote(userInitiadet:Bool){
        
        //select the most appropriate queue
        var queue = DispatchQueue.global(qos:.background)
        
        if (userInitiadet){
            queue = DispatchQueue.global(qos:.userInitiated)
        }
        
        //dispatch download async
        queue.async {
            
            //prepare query for server
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'hh:mm:ss"
            let dateToSearch = dateFormatter.string(from: Date())
            
            //prepare url for download task, only asteroids, that fell in time range between 1.1.2001 and now
            let url = NSURL(string: "\(self.nasaURL)?fall=Fell&$where=year%20between%20'\(self.startYear)-01-01T00:00:00'%20and%20'\(dateToSearch)'&$$app_token=\(self.token)")
            
            
            func callForErrorAsync(_ error:(code:Int, desc:String)){
                DispatchQueue.main.async {
                    if (self.delegate != nil){
                        self.delegate?.asterodiModel(self, failedWith: error)
                    }
                }
            }
            
            if(url == nil){
                callForErrorAsync((0, "Failed to create URL"))
                return
            }
            
            //download started synchronously in this queue
            let downloadedData = NSData(contentsOf: url as! URL)
            
            if(downloadedData == nil){
                callForErrorAsync((1, "Failed to download data from remote"))
                return
            }
            
            //parsing from json, expected particular structure
            var parsedData: Array<Dictionary<String, Any>>
            
            do {
                parsedData = try JSONSerialization.jsonObject(with: downloadedData as! Data) as! Array
            } catch {
                callForErrorAsync((2, "Failed to parse downloaded data"))
                return
            }
            
            //method, that fileters and saves data to database
            let items = self.filterAndSave(newData: parsedData)
            
            if (items == nil){
                callForErrorAsync((3, "Failed to save downloaded data"))
                return
            }
            
            //Success completation handling
            UserDefaults.standard.setValue(Date(), forKey: "kLastUpdate")
            
            DispatchQueue.main.async {
                if (self.delegate != nil){
                    self.delegate?.asteroidModel(self, found: items! as Array<Asteroid>)
                }
            }
            
        }
    }
    
    //method, that filters new data and saves them to database. returning nil indicates problem with database.
    func filterAndSave(newData:Array<Dictionary<String, Any>>!) -> Array<Asteroid>?{
        //Get asteroids already in databse
        let fetchRequest = NSFetchRequest<Asteroid>(entityName: "Asteroid")
        var asteroidsInDb: Array<Asteroid>
        do {
            asteroidsInDb = try managedObjectContext.fetch(fetchRequest)
        } catch {
            return nil
        }
        
        //Transform ids of asteroids to set
        var idOfAsteroids = Set<Int>()
        
        for asteroid in asteroidsInDb {
            idOfAsteroids.insert(Int(asteroid.id))
        }
        
        //Those asteroids, whos id is not in set are added to context to be saved to database. We also add them to array passed to calling methond
        var outputArray = Array<Asteroid>()
        
        for dict in newData {
            
            let id = Int(dict["id"] as! String)
            
            if (idOfAsteroids.contains(id!)){
                continue
            }
            
            print(dict["year"] as! String)
            
            let newEntity = NSEntityDescription.insertNewObject(forEntityName: "Asteroid", into: managedObjectContext) as! Asteroid
            
            newEntity.name = dict["name"] as? String
            newEntity.rclass = dict["recclass"] as? String
            newEntity.id = Int32(id!)
            newEntity.weigth = Double(dict["mass"] as! String)!
            newEntity.lonpos = Double(dict["reclong"] as! String)!
            newEntity.atpos = Double(dict["reclat"] as! String)!
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'hh:mm:ss.SSS Z"
            var yearString = dict["year"] as! String
            yearString = yearString + " +0000"
            
            newEntity.date = dateFormatter.date(from: yearString) as NSDate?
            
            outputArray.append(newEntity)
        }
        
        //updating database
        do {
            try managedObjectContext.save()
        }
        catch {
            return nil
        }
        
        return outputArray
    }

}
