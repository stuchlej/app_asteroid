//
//  ViewController.swift
//  app_asteroid
//
//  Created by developer on 06.02.17.
//  Copyright © 2017 Mikolas. All rights reserved.
//

import UIKit

/*
    Main view controller
 */

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, AsteroidDataModelDelegate {

    @IBOutlet weak var tableView: UITableView!
    var model:AsteroidDataModel!
    
    var dataForTable: Array<Asteroid>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let context = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
        
        //setting up model
        model = AsteroidDataModel(withCoreDataModel: context)
        model.delegate = self
        dataForTable = model.getData()
        if(dataForTable.count == 0){
            model.updateDatabase()
        }else{
            model.scheduleAutoupdating()
        }
        
        //setting up table, data should be available by now. If it's not, table will be updated using model delegate.
        tableView.dataSource = self
        tableView.delegate = self
        self.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //this method is called when data were changed
    func reloadData(){
        tableView.reloadData()
        self.title = "Seznam meteorů (\(dataForTable.count))"
    }
    
    // table view data source methods
    func numberOfSections(in tableView: UITableView) -> Int {
        
        //If there is no data now, show text instead of empty table
        if (dataForTable.count > 0 ) {
            
            self.tableView.separatorStyle = .singleLine
            return 1;
            
        } else {
            let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
            
            messageLabel.text = "Data se připravují..."
            messageLabel.textColor = UIColor.darkGray
            messageLabel.numberOfLines = 0
            messageLabel.textAlignment = .center
            messageLabel.font = UIFont(name: "System", size: 20.0)
            messageLabel.sizeToFit()
            
            self.tableView.backgroundView = messageLabel;
            self.tableView.separatorStyle = .none;
        }
        return 0;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataForTable!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //configure cells
        let newCell = tableView.dequeueReusableCell(withIdentifier: "AsteroidCell", for: indexPath) as! AsteroidTVC
        if (indexPath.row >= dataForTable.count) {
            return UITableViewCell()
        }else{
            let asteroid = dataForTable[indexPath.row]
            newCell.nameLabel.text = asteroid.name
            newCell.weightLabel.text = String(asteroid.weigth) + " g"
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy"
            newCell.yearLabel.text = dateFormatter.string(from: asteroid.date as! Date)
        }
        
        return newCell
    }
    
    //Asteroid model delegates
    func asteroidModel(_ model: AsteroidDataModel, found items: Array<Asteroid>) {
        dataForTable = model.getData()
        self.reloadData()
        
        model.scheduleAutoupdating()
    }
    
    func asterodiModel(_ model: AsteroidDataModel, failedWith error: (code: Int, desc: String)) {
        let alert = UIAlertController(title: "Chyba!", message: "data nebylo možné stáhnout", preferredStyle: .alert)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    //Clicking on cell will raise event. This event will perform particual segue. Before this segue, we parse data to view controller that shows details of asteroid
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if (segue.identifier == "detailSegue") {
            let descriptionVC = segue.destination as! DetailTVCon
            let selectedRow = self.tableView.indexPathForSelectedRow?.row
            descriptionVC.asteroid = self.dataForTable[selectedRow!]
        }
        
    }
    
}

